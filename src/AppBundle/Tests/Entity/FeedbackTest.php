<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Tests\Entity;

use AppBundle\Entity\Feedback;
use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class FeedbackTest
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Tests\Entity
 */
class FeedbackTest extends WebTestCase
{
    public function insertTest() {
        $container = $this->getContainer();
        $registry = $container->get('doctrine');
        $em = $registry->getEntityManager(null);

        $feedback = new Feedback();
        $feedback->setEmail("vladimir.eliseev@sibers.com");

        $em->persist($feedback);
        $em->flush();

        $result = $em->createQuery("SELECT f FROM AppBundle:Feedback f ".
            "WHERE f.email = :email")
            ->setParameter("name", $feedback->getEmail())
            ->getResult();

        $this->assertEqual($result[0]->getEmail(), $feedback->getEmail());
    }
}
