<?php

namespace AppBundle\Service;

use SendGrid\Mail\Mail;

/**
 * Class MailService
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Service
 */
class MailService
{
    /**
     * @var \SendGrid
     */
    protected $mailer;

        /**
         * MailService constructor.
         *
         * @param string $apiKey
         */
    public function __construct(
        $apiKey
    ) {
        $this->mailer = new \SendGrid($apiKey);
    }

    /**
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $content
     *
     * @return \SendGrid\Response|null
     * @throws \SendGrid\Mail\TypeException
     */
    public function send($from, $to, $subject, $content)
    {
        $email = new Mail();
        $email->setFrom($from);
        $email->setSubject($subject);
        $email->addTo($to);
        $email->addContent("text/plain", $content);

        $response = null;

        try {
            $response = $this->mailer->send($email);
        } catch (\Throwable $e) {
            echo 'Caught exception: '.$e->getMessage()."\n";
        }

        return $response;
    }

}
