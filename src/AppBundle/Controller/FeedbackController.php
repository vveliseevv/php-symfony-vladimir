<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Feedback;
use AppBundle\Form\FeedbackType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FeedbackController
 *
 * @Route("/feedback")
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Controller
 */
class FeedbackController extends Controller
{
    /**
     * @Route("/new", name="feedback_new")
     */
    public function newAction(Request $request)
    {
        $feedback     = new Feedback();
        $feedbackForm = $this->createForm(FeedbackType::class, $feedback);
        $em           = $this->getDoctrine()->getManager();

        $feedbackForm->handleRequest($request);

        if ($feedbackForm->isSubmitted() && $feedbackForm->isValid()) {
            $this->addFlash(
                'notice',
                'Your feedback was saved!'
            );

            $em->persist($feedback);
            $em->flush();

            $this->redirectToRoute('feedback_new');
        }


        return $this->render(
            '@App/Feedback/new.html.twig', [
            'form' => $feedbackForm->createView(),
        ]
        );
    }

}
