This is a test
========================

To run the project you need to do the following
--------------

```
  
  $ git clone git@bitbucket.org:vveliseevv/php-symfony-vladimir.git

  $ cd php-symfony-vladimir

  $ composer install
  
  $ php bin/console doctrine:database:create

  $ php bin/console assets:install --symlink
  
  $ php bin/console doctrine:migrations:migrate
  
  $ php bin/console fos:user:create adminuser --super-admin
  
  $ php bin/console s:r
```
