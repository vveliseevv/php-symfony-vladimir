<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\Feedback;
use AppBundle\Service\MailService;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Monolog\Logger;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FeedbackListener
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\EventListener
 */
class FeedbackListener
{
    /**
     * @var MailService
     */
    protected $mailer;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    protected $fromEmail;

    /**
     * @var string
     */
    protected $subjectEmail;

    /**
     * FeedbackListener constructor.
     *
     * @param MailService     $mailer
     * @param LoggerInterface $logger
     * @param                 $fromEmail
     * @param                 $subjectEmail
     */
    public function __construct(
        MailService $mailer, LoggerInterface $logger, $fromEmail, $subjectEmail
    ) {
        $this->mailer       = $mailer;
        $this->logger       = $logger;
        $this->fromEmail    = $fromEmail;
        $this->subjectEmail = $subjectEmail;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ( ! $entity instanceof Feedback) {
            return;
        }

        $entityManager = $args->getObjectManager();
        $response      = null;

        $result = $entityManager->getRepository(Feedback::class)->findBy(
            ['email' => $entity->getEmail()], ['createdAt' => 'DESC'], 1
        );

        try {
            if ( ! $result || ($result && $this->allowedToSend($result[0]->getCreatedAt()))) {
                $response = $this->mailer->send(
                    $this->fromEmail, $entity->getEmail(), $this->subjectEmail,
                    $entity->getMessage()
                );
            }

            if ($response->statusCode() == Response::HTTP_OK
                || $response->statusCode() == Response::HTTP_ACCEPTED
            ) {
                $entity->setNotificationSent(true);
            }
        } catch (\Throwable $ex) {
            $this->logger->error('Failed to send message'.print_r($response));
        }
    }


    /**
     * @param \DateTime $date
     *
     * @return bool
     * @throws \Exception
     */
    private function allowedToSend($date)
    {
        $currentDatetime = new \DateTime();
        $interval        = $currentDatetime->diff($date);
        $diff            = ($interval->days * 24) + $interval->h;

        return $diff >= 1;
    }
}
